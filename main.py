from craiyon import Craiyon
from PIL import Image, ImageDraw, ImageFont
from io import BytesIO
import base64
import uuid
from wonderwords import RandomWord

r = RandomWord()
def random_pos(pos):
    return r.word(include_parts_of_speech=[pos])

generator = Craiyon() # Instantiates the api wrapper
prompt = " ".join([random_pos("noun"), random_pos("verb")+"ing", random_pos("adjective"), random_pos("noun")])
result = generator.generate(prompt) # Generates 9 images by default and you cannot change that
images = result.images # A list containing image data as base64 encoded strings
image_size = 250
number_per_axis = 3
new_xy = number_per_axis * image_size
text_margin = 100
new = Image.new("RGBA", (new_xy, new_xy+text_margin))
index = 0
for i in images:
    myuuid = uuid.uuid4()
    image = Image.open(BytesIO(base64.decodebytes(i.encode("utf-8"))))
    image.save("/tmp/cra-"+str(myuuid)+".png")
    x = image_size*(index % 3)
    y = image_size*int(index / 3)
    new.paste(image, (x,y))
    index+=1
art_uuid = str(uuid.uuid4())
new_y_smol = int((new_xy+text_margin)/2)
new_x_smol = int(new_xy/2)

new = new.resize((new_x_smol, new_y_smol))
d = ImageDraw.Draw(new)
d.text((10,new_xy+10), prompt, fill=(255,0,0,255))
new.save(f"{prompt.replace(' ', '-')}-{art_uuid}.png")
print(art_uuid)
